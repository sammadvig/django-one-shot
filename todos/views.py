from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def todo_list(request):
    todo_list_list = TodoList.objects.all
    context = {
        "todo_list_list": todo_list_list,
    }
    return render(request, "todos/list_list.html", context)


def todo_detail(request, id):
    todo_object = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": todo_object
    }
    return render(request, "todos/details.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            list.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()

    context = {
            "form": form,
    }
    return render(request, "todos/create_list.html", context)


def edit_todo_list(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id)
    else:
        form = TodoListForm(instance=post)
    context = {
        "todo_object": post,
        "form": form
    }
    return render(request, "todos/edit.html", context)


def delete_todo_list(request, id):
    todo_list_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            list = form.save()
            list.save()
            return redirect("todo_list_detail", id=list.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create_item.html", context)


def edit_todo_item(request, id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=post)
        if form.is_valid():
            item = form.save()
            item.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=post)
    context = {
        "todo_object": post,
        "form": form
    }
    return render(request, "todos/edit_item.html", context)
