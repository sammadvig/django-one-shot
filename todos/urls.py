from django.urls import path
from todos.views import todo_list, todo_detail, create_todo_list, edit_todo_list, delete_todo_list, create_todo_item, edit_todo_item


urlpatterns = [
    path("", todo_list, name="todo_list_list"),
    path("<int:id>/", todo_detail, name="todo_list_detail"),
    path("create/", create_todo_list, name="todo_list_create"),
    path("<int:id>/edit/", edit_todo_list, name="todo_list_update"),
    path("<int:id>/delete/", delete_todo_list, name="todo_list_delete"),
    path("items/create/", create_todo_item, name="todo_item_create"),
    path("items/<int:id>/edit/", edit_todo_item, name="todo_item_update"),
]
